from report import *

class publishers(Command):
    args = '[ toc ] self'
    def invoke(self, tex):
        Command.invoke(self, tex)
        if not self.ownerDocument.userdata.has_key('publishers'):
            self.ownerDocument.userdata['publishers'] = self

class dedication(Command):
    args = '[ toc ] self'
    def invoke(self, tex):
        Command.invoke(self, tex)
        if not self.ownerDocument.userdata.has_key('dedication'):
            self.ownerDocument.userdata['dedication'] = self

class uppertitleback(Command):
    args = '[ toc ] self'
    def invoke(self, tex):
        Command.invoke(self, tex)
        if not self.ownerDocument.userdata.has_key('uppertitleback'):
            self.ownerDocument.userdata['uppertitleback'] = self

class lowertitleback(Command):
    args = '[ toc ] self'
    def invoke(self, tex):
        Command.invoke(self, tex)
        if not self.ownerDocument.userdata.has_key('lowertitleback'):
            self.ownerDocument.userdata['lowertitleback'] = self

class marginline(Command):
    args = 'text'

class minisec(Command):
    args = 'self'
