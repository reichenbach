

from plasTeX.Renderers import Renderer as BaseRenderer
import textwrap, re

class FancyWrapper(textwrap.TextWrapper):
    """Improved version of the textwrap library"""

    wordsep_simple_re = re.compile(r'( +)')
    wordsep_re = re.compile(
        r'( +|'                                  # any whitespace
        r'[^ \w]*\w+[^0-9\W]-(?=\w+[^0-9\W])|'   # hyphenated words
        r'(?<=[\w\!\"\'\&\.\,\?])-{2,}(?=\w))')   # em-dash

    def __init__(self, **kwargs):
        textwrap.TextWrapper.__init__(self, drop_whitespace=False, **kwargs)

#        ulpat = u'\u02cd?[^ \u02cd]*[^ \u02cd\u201e]\u02cd'
        ulpat = u'\u02cd?[^ \u02cd]+\u02cd[,\\.;\\?]*'

        self.wordsep_re_uni = re.compile(u'( +|' + ulpat + r')', re.U)
        self.wordsep_simple_re_uni = re.compile(
            r'( +|'                                  # any whitespace
            + ulpat +
            r'[^ \w]*\w+[^0-9\W]-(?=\w+[^0-9\W])|'   # hyphenated words
            r'(?<=[\w\!\"\'\&\.\,\?])-{2,}(?=\w))'   # em-dash
            , re.U)

    def _indent(self, line):
        return self.subsequent_indent + ''.join(line)

    def _wrap_chunks(self, chunks):
        """_wrap_chunks(chunks : [string]) -> [string]

        Wrap a sequence of text chunks and return a list of lines of
        length 'self.width' or less.  (If 'break_long_words' is false,
        some lines may be longer than this.)  Chunks correspond roughly
        to words and the whitespace between them: each chunk is
        indivisible (modulo 'break_long_words'), but a line break can
        come between any two chunks.  Chunks should not have internal
        whitespace; ie. a chunk is either all whitespace or a "word".
        Whitespace chunks will be removed from the beginning and end of
        lines, but apart from that whitespace is preserved.
        """
        lines = []
        if self.width <= 0:
            raise ValueError("invalid width %r (must be > 0)" % self.width)

        # Arrange in reverse order so items can be efficiently popped
        # from a stack of chucks.
        chunks.reverse()
        last_len = 0

        while chunks:

            # Start the list of chunks that will make up the current line.
            # cur_len is just the length of all the chunks in cur_line.
            cur_line = []
            cur_len = 0

            # Figure out which static string will prefix this line.
            if lines:
                indent = self.subsequent_indent
            else:
                indent = self.initial_indent

            # Maximum width for this line.
            width = self.width - len(indent)

            # First chunk on line is whitespace -- drop it, unless this
            # is the very beginning of the text (ie. no lines started yet).
            if self.drop_whitespace and chunks[-1].strip() == '' and lines:
                del chunks[-1]

            while chunks:
                l = len(chunks[-1])

                # Can at least squeeze this chunk onto the current line.
                if cur_len + l <= width:
                    cur_line.append(chunks.pop())
                    cur_len += l

                # Nope, this line is full.
                else:
                    break

            # The current line is full, and the next chunk is too big to
            # fit on *any* line (not just this one).
            if chunks and len(chunks[-1]) > width:
                self._handle_long_word(chunks, cur_line, cur_len, width)
            # Balance line lengths -- better would be to review even older lines
            elif chunks and last_len != 0:
                l = len(lines[-1][-1])
                while last_len - 2*l > cur_len:
                    cur_line = [lines[-1][-1]] + cur_line
                    del lines[-1][-1]
                    cur_len +=l
                    last_len -= l
                    l = len(lines[-1][-1])


            if cur_line:
                lines.append(cur_line)
                last_len = cur_len

        if lines == []:
            return []

        for line in lines:
            while line and line[0].isspace():
                del line[0]
            while line and line[-1].isspace():
                del line[-1]
            if line and line[0].count(u'\u02cd') == 1:
                line[0] = u'\u02cd' + line[0]

        return ([self.initial_indent + ''.join(lines[0])] + 
            map(self._indent, lines[1:]))

#    def wrap(self, text):
#        return textwrap.TextWrapper.wrap(self, text.lstrip())

class GutenbergRenderer(BaseRenderer):
    """ Renderer for plain text documents for upload to Project Gutenberg"""

    outputType = unicode
    fileExtension = '.utf8'
    lineWidth = 72
    underlinesym = u'\u02cd'   # Makron
    
    aliases = {
        'superscript': 'active::^',
        'subscript': 'active::_',
        'dollar': '$',
        'percent': '%',
        'opencurly': '{',
        'closecurly': '}',
        'underscore': '_',
        'ampersand': '&',
        'hashmark': '#',
        'space': ' ',
        'tilde': 'active::~',
    }
    
    def __init__(self, *args, **kwargs):
        BaseRenderer.__init__(self, *args, **kwargs)
        
        # Load dictionary with methods
        for key in vars(type(self)):
            if key.startswith('do__'):
                self[self.aliases[key[4:]]] = getattr(self, key)
            elif key.startswith('do_'):
                self[key[3:]] = getattr(self, key)

        self['default-layout'] = self['document-layout'] = self.default
        
        self.footnotes = []
        self.blocks = []
	self.Footnotes = []

    def unify_math_spaces(self, s):
        s = re.sub(u' \u00b7', u'\u00b7', s)
        return re.sub(r'(\S)=', r'\1 =', s)

    def addBlock(self, s):
        """ Create a new block level element placeholder """
        self.blocks.append(s)
        width = max([len(x) for x in s.split('\n')])
        return u'\001[%s%s]' % (len(self.blocks)-1, '@'*width)

    def default(self, node):
        """ Rendering method for all non-text nodes """
        # Handle characters like \&, \$, \%, etc.
        if len(node.nodeName) == 1 and node.nodeName not in string.letters:
            return self.textDefault(node.nodeName)
            
        # Render child nodes
        return unicode(node)

    def processFileContent(self, document, s):
        s = BaseRenderer.processFileContent(self, document, s)

        # Put block level elements back in
        block_re = re.compile('(\\s*)\001\\[(\\d+)@+\\]')
        while 1:
            m = block_re.search(s)
            if not m:
                break

            space = ''
            before = m.group(1)
            if before is None:
                before = ''
            if '\n' in before:
                spaces = before.split('\n')
                space = spaces.pop()
                before = '\n'.join(spaces) + '\n'

            block = self.blocks[int(m.group(2))]
            block = space + block.replace('\n', u'\n%s' % space) 

            s = block_re.sub('%s%s' % (before, block), s, 1)

        # Clean up newlines
        #return re.sub(r'\s*\n\s*\n(\s*\n)+', r'\n\n\n', s)
        return s
    
    def textDefault(self, node):
        return unicode(node)

    def postProcessText(self, s):
        #return re.sub(self.underlinesym+'+', '_', s)
        return re.sub(u'\u02cd+', '_', s)

    def wrap(self, s, **kwargs):
	w = FancyWrapper(width=self.lineWidth, break_long_words=False, **kwargs)
	return map(self.postProcessText, w.wrap(unicode(s)))
    
    def fill(self, s, **kwargs):
	w = FancyWrapper(width=self.lineWidth, break_long_words=False, **kwargs)
	return self.postProcessText(w.fill(unicode(s)))
    
    # Alignment

    def do_flushleft(self, node):
        return self.fill(unicode(node)).strip()
        
    do_raggedbottom = do_raggedright = do_leftline = do_flushleft
    
    def center(self, text):
        s = self.wrap(text)
        for i, line in enumerate(s):
            indent = int((self.lineWidth - len(line)) / 2)
            s[i] = '%s%s' % (' '*indent, line)
        return '\n'.join(s)
            
    def do_center(self, node):
        return self.center(unicode(node))
        
    do_centering = do_centerline = do_center

    def do_flushright(self, node):
        s = self.wrap(node)
        for i, line in enumerate(s):
            indent = self.lineWidth - len(line)
            s[i] = '%s%s' % (' '*indent, line)
        return '\n'.join(s).rstrip()
        
    do_raggedleft = do_llap = do_flushright
    
    # Arrays
    
    def do_array(self, node, cellspacing=(2,1), render=unicode):
        # Render the table cells and get min and max column widths
        colwidths = []
        for r, row in enumerate(node):
            for c, cell in enumerate(row):
                if isinstance(render, basestring):
                    s = getattr(cell, render)().strip()
                else:
                    s = render(cell).strip()
                maxlength = max([len(x) for x in s.split('\n')])
                minlength = min([len(x) for x in s.split() if x.strip()])
                if r == 0:
                    colwidths.append([minlength, maxlength])
                else:
                    colwidths[c] = [max(minlength, colwidths[c][0]), 
                                    max(maxlength, colwidths[c][1])]

        # Determine best column widths
        maxline = self.lineWidth - len(colwidths)*cellspacing[0]
        minwidths = [x[0] for x in colwidths] # minimums
        maxwidths = [x[1] for x in colwidths] # maximums
        if sum(maxwidths) < maxline:
            outwidths = maxwidths
        elif sum(minwidths) > maxline:
            outwidths = minwidths
        else:
            outwidths = list(maxwidths)
            # If the minimum is also the maximum, take it out of the
            # algorithm to determine lengths.
            for i, item in enumerate(maxwidths):
                if maxwidths[i] == minwidths[i]:
                    maxwidths[i] = -1
            # Iteratively subtract one from the longest line until the
            # table will fit within maxline.
            while sum(outwidths) > maxline:
                index = maxwidths.index(max(maxwidths))
                maxwidths[i] -= 1
                outwidths[i] -= 1
                if maxwidths[i] == minwidths[i]:
                    maxwidths[i] = -1

        # Render cells to correct widths
        rendered = []
        for r, row in enumerate(node):
            current = []
            rendered.append(current)
            for c, cell in enumerate(row):
                origwidth = self.lineWidth
                self.lineWidth = outwidths[c]
                if isinstance(render, basestring):
                    s = getattr(cell, render)().split('\n')
                else:
                    s = render(cell).strip().split('\n')
                if s and r == 0 and c == 0:
                    s[0] = s[0].lstrip()
                current.append((max([len(x) for x in s]), len(s), s))
                self.lineWidth = origwidth

        # Pad cells to fill out a block
        for r, row in enumerate(rendered):
            linesneeded = max([x[1] for x in row]) + cellspacing[1]
            for c, cell in enumerate(row):
                width, height, content = cell
                # Add the appropriate number of lines
                for i in range(linesneeded - len(content)):
                    content.append(' '*width)
                # Pad all lines to the same length
                for i, line in enumerate(content):
                    content[i] = content[i] + ' '*(outwidths[c]-width+cellspacing[0])
                rendered[r][c] = content
        
        # Piece all of the table parts together
        output = []
        for row in rendered:
            # Continue until cell content is empty (the last cell in this case)
            while row[-1]:
                for cell in row:
                    output.append(cell.pop(0))
                # Get rid of unneeded whitespace and put in a line break
                output[-1] = output[-1].strip()
                output.append('\n')
            
        return ''.join(output)
        
    do_tabular = do_tabularx = do_longtable = do_array
    
    def do_cline(self, node):
        return ''
        
    def do_multicolumn(self, node):
        return unicode(node)

    # Bibliography
    def do_thebibliography(self, node):
        output = ['', 'Bibliography', '']
        for item in node:
            bullet = u'[%s] ' % item.bibcite
            bulletlen = len(bullet)
            output.append(self.fill(item, initial_indent=bullet,
                                        subsequent_indent=' '*bulletlen))
            output.append('')
        output.append('')
        return u'\n'.join(output)        

    def do_bibliographystyle(self, node):
        return u''

    def do_bibliography(self, node):
        return self.default(node)
  
    def do_cite(self, node):
        output = []
        for item in node.citation():
            output.append(unicode(item))
        return u''.join(output)

    def do_bibliographyref(self, node):
        return self.default(node)

    # Boxes
    
    do_mbax = do_makebox = do_fbox = do_framebox = do_parbox = default
    do_minipage = do_raisebox = do_rule = default
    
    # Breaking
    
    def do_linebreak(self, node):
        return u'\n\n'
        
    do_newline = do_pagebreak = do_newpage = do_clearpage = do_cleardoublepage = do_linebreak

    # Crossref
    
    def do_ref(self, node):
        return unicode(node.idref['label'].ref)

    def do_pageref(self, node):
        return u'*'

    def do_label(self, node):
        return u''

    # Floats
    
    def do_figure(self, node):
        return unicode(node)
        
    do_table = do_marginpar = do_figure

    def do_caption(self, node):
        return u'%s %s: %s' % (node.title, node.ref, unicode(node))

    # Font Selection
    
    do_mdseries = do_textmd = do_bfseries = do_textbf = default
    do_rmfamily = do_textrm = do_sffamily = do_textsf = default
    do_ttfamily = do_texttt = do_upshape = do_textup = default
    do_itshape = do_textit = do_scshape = do_textsc = default
    do_slshape = do_textsl = do_textnormal = do_rm = default
    do_cal = do_it = do_sl = do_bf = do_tt = do_sc = default
    do_tiny = do_scriptsize = do_footnotesize = do_small = default
    do_normalsize = do_large = do_Large = do_LARGE = do_huge = do_HUGE = default

    def do_symbol(self, node):
        return u'*'

    # Footnotes
    def do_footnote(self, node):
        mark = u'[%s]' % (len(self.footnotes)+1)
        self.footnotes.append(self.fill(node, initial_indent='%s ' % mark,
                                     subsequent_indent=' ' * (len(mark)+1)).strip())
        return mark
        
    def do_footnotetext(self, node):
        self.do_footnote(self, node)
        return ''
    
    def do_footnotemark(self, node):
        return u'[%s]' % (len(self.footnotes)+1)

    def do_Footnote(self, node):
        mark = u'[%s]' % unicode(node.attributes['mark'])
        self.Footnotes.append(self.fill(node.attributes['text'],
                    initial_indent='%s ' % mark,
                    subsequent_indent=' ' * (len(mark)+1)).strip())
        return mark

    # Index
    
    def do_theindex(self, node):
        return u''
        
    do_printindex = do_index = do_theindex
    
    # Lists

    def do_itemize(self, node):
        output = []
        for item in node:
            bullet = '   * '
            bulletlen = len(bullet)
            output.append(self.fill(item, initial_indent=bullet,
                                        subsequent_indent=' '*bulletlen))
        return self.addBlock(u'\n'.join(output))
        
    def do_enumerate(self, node):
        output = []
        for i, item in enumerate(node):
            bullet = '   %d. ' % (i+1)
            bulletlen = len(bullet)
            output.append(self.fill(item, initial_indent=bullet,
                                        subsequent_indent=' '*bulletlen))
        return self.addBlock(u'\n'.join(output))
        
    def do_description(self, node):
        output = []
        for item in node:
            bullet = '   %s - ' % item.attributes.get('term','')
            bulletlen = len(bullet)
            output.append(self.fill(item, initial_indent=bullet,
                                        subsequent_indent='      '))
        return self.addBlock(u'\n'.join(output))

    do_list = do_trivlist = do_description
    
    # Math
    
    def do_math(self, node):
        # Remove _ before simple Numbers: x_3 -> x3
        s = re.sub(u'([^\u220f\u2210\u2211\u222b\u222e])_(\d)', r'\1\2',
            unicode(node))
        return re.sub(u' ', u'\u00a0', self.unify_math_spaces(s))

    do_ensuremath = do_math
    
    def do_equation(self, node):
        #s = u'   %s' % re.compile(r'^\s*\S+\s*(.*?)\s*\S+\s*$', re.S).sub(r'\1', node.source)
        #return re.sub(r'\s*(_|\^)\s*', r'\1', s)
        return self.center(self.do_math(node))

    do_displaymath = do_equation
    
    def do_eqnarray(self, node):
        def render(node):
            s = re.compile(r'^\$\\displaystyle\s*(.*?)\s*\$\s*$', re.S).sub(r'\1', node.source)
            return re.sub(r'\s*(_|\^)\s*', r'\1', s)
        s = self.do_array(node, cellspacing=(1,1), render=render)
        output = []
        for line in s.split('\n'):
            output.append('   %s' % line)
        return u'\n'.join(output)     

    do_align = do_gather = do_falign = do_multiline = do_eqnarray 
    do_multline = do_alignat = do_split = do_eqnarray
    
    # Misc
    
    do_bgroup = default
    
    def do_def(self, node):
        return u''
    
    do_tableofcontents = do_input = do_protect = do_let = do_def
    do_newcommand = do_hfill = do_hline = do_openout = do_renewcommand = do_def
    do_write = do_hspace = do_appendix = do_global = do_noindent = do_def
    do_include = do_markboth = do_setcounter = do_refstepcounter = do_def
    do_medskip = do_smallskip = do_parindent = do_indent = do_setlength = do_def
    do_settowidth = do_addtolength = do_nopagebreak = do_newwrite = do_def
    do_newcounter = do_typeout = do_sloppypar = do_hfil = do_thispagestyle = do_def

    def do_egroup(self, node):
        return u''
    
    # Pictures
    
    def do_picture(self, node):
        return u''
        
    # Primitives
    
    def do_par(self, node):
        numchildren = len(node.childNodes)
        if numchildren == 1 and not isinstance(node[0], basestring):
            return u'%s\n\n' % unicode(node)
        elif numchildren == 2 and isinstance(node[1], basestring) and \
           not node[1].strip():
            return u'%s\n\n' % unicode(node)
        s = u'%s\n\n' % self.fill(node)
	if self.Footnotes:
#	if Footnotes:
            Footnotes = u'\n\n'.join(self.Footnotes)
            self.Footnotes = []
            s = u'%s%s\n\n' % (s, Footnotes)
        if not s.strip():
            return u''
        return s

    def do__superscript(self, node):
        #return self.default(node)
        s = self.default(node)
        if s.startswith(u'\u2032'):
            return s
	return '^'+s

    # TODO: More special cases - like string lenght?
    def do__subscript(self, node):
        s = self.default(node)
	#if s.isdigit():
        #    return s
        return '_'+s

    def do_mathrm(self, node):
        return self.default(node)
    
    # Quotations
    
    def do_quote(self, node):
        return self.center(self, node)
    
    do_quotation = do_verse = do_quote

    # Sectioning

    def do_document(self, node):
        content = unicode(node).rstrip()
        footnotes = u'\n\n'.join(self.footnotes).rstrip()
        if footnotes:
            content = u'%s\n\n\n%s' % (content, footnotes)
        return u'%s\n\n' % content
        
    def do_maketitle(self, node):
        output = []
        metadata = node.ownerDocument.userdata
        if 'title' in metadata:
            output.append(self.center(metadata['title']).rstrip().upper()+u'\n')
        if 'author' in metadata:
            output += map(unicode.rstrip, map(self.center, metadata['author']))
            output[-1] += u'\n'
        if 'date' in metadata:
            output.append(self.center(metadata['date']).rstrip())
        if 'publishers' in metadata:
            output.append(self.center(metadata['publishers']).rstrip())
        if 'thanks' in metadata:
            output.append(self.center(metadata['thanks']).rstrip())
        if 'uppertitleback' in metadata or 'lowertitleback' in metadata:
            output.append(u'\n\n')
        if 'uppertitleback' in metadata:
            output.append(self.fill(unicode(metadata['uppertitleback'])))
        if 'lowertitleback' in metadata:
            output.append(u'\n%s' % self.fill(unicode(metadata['lowertitleback'])))
        if 'dedication' in metadata:
            output.append(u'\n\n\n')
            output.append(self.center(metadata['dedication']).rstrip())
        return u'\n'.join(output)
#        return u'\n%s\n\n' % u'\n'.join(output)

# Workaround: Hardcode disabling of autogenerated numbering
    def do_section(self, node):
        return u'\n\n\n%s' % (u'%s\n\n%s' % (self.fill(node.title), node)).strip()

    do_subsection = do_subsubsection = do_section
    do_paragraph = do_subparagraph = do_subsubparagraph = do_section

# Workaround: Hardcode disabling of autogenerated numbering
    def do_chapter(self, node):
        return u'\n\n\n\n\n%s' % (u'%s\n\n\n%s' % (self.fill(node.title), node)).strip()

    do_part = do_chapter

    def do_title(self, node):
        return u''
        
    do_dedication = do_publishers = do_author = do_date = do_thanks = do_title
    do_uppertitleback = do_lowertitleback = do_title
    
    def do_abstract(self, node):
        return self.center(node)

    # Sentences

    def do__dollar(self, node):
        return u'$'
        
    def do__percent(self, node):
        return u'%'
        
    def do__opencurly(self, node):
        return u'{'
        
    def do__closecurly(self, node):
        return u'}'
    
    def do__underscore(self, node):
        return u'_'
        
    def do__ampersand(self, node):
        return u'&'
        
    def do__hashmark(self, node):
        return u'#'
    
    def do__space(self, node):
        return u' '

    def do_LaTeX(self, node):
        return u'LaTeX'

    def do_TeX(self, node):
        return u'TeX'

    def do_spacedout(self, node):
        return re.sub(r'(\S)', r'\1'+u'\u202f', re.sub(r'(\s)', r'\1 ', self.default(node)))
    
    def do_underline(self, node):
        #return u'_%s_' % re.sub(r'\s', r'_', self.default(node))
        #return u'_%s_' % self.default(node)
        return (self.underlinesym + 
            re.sub(r'\s', self.underlinesym, self.default(node)) +
            self.underlinesym)

    do_em = do_emph = do_underline

    def do__tilde(self, node):
        return u' '

    def do_enspace(self, node):
        return u' '

    do_quad = do_qquad = do_enspace
        
    def do_enskip(self, node):
        return u''

    do_thinspace = do_enskip

    def do_underbar(self, node):
        return self.default(node)

    # Space
    
    def do_hspace(self, node):
        return u' '
    
    def do_vspace(self, node):
        return u''
        
    do_bigskip = do_medskip = do_smallskip = do_vspace
    
    # Tabbing - not implemented yet
    
    # Verbatim
    
    def do_verbatim(self, node):
        return re.sub(r'^\s*\n', r'', unicode(node)).rstrip()

    do_alltt = do_verbatim

    def do_marginline(self, node):
        return u''

Renderer = GutenbergRenderer
